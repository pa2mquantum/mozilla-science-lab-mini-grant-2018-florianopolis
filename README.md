# Mozilla Science Mini Grants 2017

This is my proposal to Mozilla Science Mini Grants 2017 Grant to develop a Python Curriculum around Oceanography.

## Grant Announcement

https://science.mozilla.org/blog/2018-mini-grant-rfp

> ## What
>
> We’re looking for proposals for projects focusing on Prototyping, Community Building, and/or Curriculum that enhance our broader community efforts toward open innovation, efficiency in regards to practicing open science (lower barriers, ease of use & integration, etc), and reproducibility (transparent research methods & results).
>
> ## How Much
>
> Grant awards will range from $2,000 – $5,000 USD.
>
> When
>
> Grant-funded projects or activities should take place between April 1, 2018 and September 30, 2018.  Applications may be submitted for projects lasting up to 6 months.  Final reports will be due October 22nd, 2018.
>
> Deadline for Letters of Intent will be Sunday, December 15th, 2017.  Invitations for full proposals will be sent the week of January 5th, 2018.  Deadlines for full proposals will be January 29th, 2018.  Award notification by the week of March 5th, 2018.
>
> The full RFP, the link to the application form, and link to the application guide are available below.

## Repository Structure

- budget.ods

  Contains the budget. The original version is a copy of [the template provided by Mozilla Science](https://docs.google.com/spreadsheets/d/1mImau7-hg1CuIKmKwMF66wXFj3uKSnel1SjCOK2794s/edit#gid=0).
- full-proposal.md

  Contains the full proposal. Application guide can be found [here](https://docs.google.com/document/d/1qPt0NWDIBmZ10F9bsWPGbDeZfy6pl2k177fV4jSoddo/edit#bookmark=id.7hw0titrpfvh).
- letter-of-intent.md

  Contains the letter of intent. Application guide can be found [here](https://docs.google.com/document/d/1qPt0NWDIBmZ10F9bsWPGbDeZfy6pl2k177fV4jSoddo/edit#bookmark=kix.wo54w61pe2b4).

## How to Contribute with the Proposal

Use any of the following actions:

- send a pull request,
- open issues, or
- request (by issue) to be added as contributor with writing access.