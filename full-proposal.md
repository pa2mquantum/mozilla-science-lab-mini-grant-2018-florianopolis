# Abstract

The purpose of our project is to help Brazilian Oceanography, Biology, Geosciences and Meteorology researchers, PhD students and undergraduate students
on their adoption of Free and Open Source Software (FOSS) in their research and in turn help them enter the world of Open Science.

We will achieve this aim by creating a 12-hours-long Python Curriculum, in Portuguese, focused on the above areas, and by leveraging local 
[Research Software Engineer](http://rse.ac.uk/what-is-an-rse/) champions who will help deliver a trial of the developed curriculum.

# Project Description

## Background

Melissa Weber Mendonça is a Professor at [Federal University of Santa Catarina](http://ufsc.br)'s Mathematics department since 2010. As a mathematical 
programming and scientific computing teacher and researcher, she has collaborated with the Oceanography department of her home institution for the last 3 years.
In 2016, she co-organized and taught a [Python workshop for Oceanographers and Biologists](https://github.com/melissawm/oceanobiopython). At the time, the 
demand for the workshop was very high: there were 30 participant slots available and more than 100 people registered to take part in it. The Oceanography 
department was interested in organizing it again, this time with a more specialized focus on applications related to Oceanography and Geosciences.

Many scientists in Brazil are not using Python because most universities teach MATLAB as the main tool for analysis and other computational applications. However,
there is a general understanding that using a proprietary system is not ideal for Open Science, and there is a great interest in Python as a language. 
Unfortunately, teaching materials in Portuguese are still scarce, especially for people who are not particularly skilled in programming. Not having materials
in Portuguese is one of the barriers to enter the computer field as mentioned by Andy Oram at [Open Source in Brazil](http://www.oreilly.com/programming/free/open-source-in-brazil.csp).
This project aims to reduce the barriers described above by financing a five days lesson curriculum creation sprint.
The new curriculum will be available under the Creative Commons Attribution 4.0 license
allowing more students and researchers Portuguese speakers to benefit from this project.

## Proposed Activities Summary

In this project we will

- Coordinate a lesson curriculum creating sprint;
- Release a lesson curriculum covering the basics of Python and related libraries for Oceanography, Biology, Geosciences and Meteorology;
- Run a 12-hour pilot workshop using the curriculum.

## Lesson Curriculum Creation Sprint

We will invite and coordinate a group of professors, researchers, research software engineers, software developers and educators to create, during 9-13 April, a 
curriculum for scientific Python and tools relevant to the areas mentioned above. The curriculum will be designed to educate mostly grad students, 
but will be suitable to senior researchers and last year undegraduate students.

The working group will be lead by Melissa Weber Mendonça and Juliana Leonel, two full-time professors at the Federal Univesity of Santa Catarina.
Melissa and Juliana will host the sprint in Florinópolis, Brazil
to increase the engagement of the local community.
They can book rooms at Federal Univesity of Santa Catarina for free which makes this project more cost efficient.

As external specialists, Melissa and Juliana will invite Filipe Fernandes, Tania Allard, and Alejandra Gonzalez Beltran.

Filipe Fernandes is a long time open source developer working in Oceanography. FIXME

Tania Allard is a Research Software Engineer with experience developing lesson content for the OpenDreamKit project as well a for OOOMinds, a Leverhulme fellowship project. 
Tania has significant experience working on multi-disciplinary projects as well as in data and software engineering. 
As a research software engineer she is particularly involved in the development of complex data analysis workflows as well as adequate data curation and preservation strategies. 
She is particularly interested in the reproducible and replicable scientific research, and thus has working experience in developing standards and tools for researchers and data scientists
to ensure best practices in their workflows while maximising their research impact. 
In addition, she has over 8 years teaching experience at different levels of education and has taken courses in pedagogy and teaching methodologies. 

Alejandra Gonzalez Beltran is a Research Lecturer at the Oxford e-Research Centre, Department of Engineering Science at the University of Oxford.
Alejandra has experience data science and software engineering, including expertise on data standards and knowledge management working on multi-disciplinary
domains, including biomedical sciences and computer science. She also has vast experience in teaching in different levels of education, is a Carpentry Instructor and will complete the Instructor Trainer
training in early 2018. Alejandra's background is in Computer Science, having received a Licentiateship in Computer Science (would be equivalent to an MSc)
from National University of Rosario, Argentina and a PhD in Computer Science from Queen's University Belfast, UK.

## Lesson Curriculum Release

After the sprint,
the authors will continue to collaborate on GitLab
polishing the curriculum
until the release planned for end of July, 2018.

Raniere Silva and Aleksandra Pawlik have committed to be external reviewers for the curriculum.

## Pilot Workshop

The pilot workshop will be hosted at the Federal University of Santa Catarina in August, 2018.
Seats will be offered to Oceanography, Biology, Geosciences and Meteorology researchers
after a selection process.
The pilot workshop will be free of charge to learners
and at the end we will host a dinner to collect feedback.

## Future work

After the pilot included in this proposal, the chairs of this proposal will continue improving the curriculum and delivering it in different formats at
their home institution. Initially, this would include an 12-hour workshop offered every semester, and a possibly shorter version to be offered at Oceanography conferences. 
There is also a demand for the inclusion of this course or some variation of it as a permanent semester-long course in the Oceanography 
department, and maybe others. There are also plans to release the same material in other languages, such as English and Spanish.

# Benefit to Project and Community

With this project we will encourage students and researchers in Oceanography, Biology, Geosciences and Meteorology to use open source tools and follow open science best practices.

# Schedule

- Lesson Curriculum Creation Workshop
- 

# Project Team

+--------------------+--------------------+-------------------------------+----------------------------+
|Name                |Institution         |E-Mail                         |Contribution to and         |
|                    |                    |                               |role                        |
+====================+====================+===============================+============================+
|Melissa Weber       |Federal University  |melissawm@gmail.com            |Co-Chair, local organiser,  |
|Mendonça            |of Santa Catarina   |                               |lesson creator, lead        |
|                    |                    |                               |instructor.                 |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Juliana Leonel      |Federal University  |juoceano@gmail.com             |Co-Chair, local organiser,  |
|                    |of Santa Catarina   |                               |lesson creator.             |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Filipe Fernandes    |Southeast Coastal   |ocefpaf@gmail.com              |Python Software Engineer    |
|                    |Ocean Observing     |                               |consultant with experience  |
|                    |Regional Association|                               |in Oceanography.            |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Tania Allard        |University of       | t.allard@sheffield.ac.uk      |Researcher Software Engineer|
|                    |Sheffield           |                               |consultant in Python        |
|                    |                    |                               |training.                   |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Alejandra Gonzalez  | University of      | alejandra.gonzalezbeltran@    |Researcher Software Engineer|
|Beltran             | Oxford             | oerc.ox.ac.uk                 |consutant in data           |
|                    |                    |                               |management.                 |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Raniere Silva       |University of       |raniere@rgaiacs.com            |Lesson creator advisor.     |
|                    |Manchester          |                               |                            |
|                    |                    |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+
|Aleksandra Pawlik   |New Zealand eScience|aleksandra.pawlik@gmail.com    |Lesson creator advisor.     |
|                    |Infrastructure      |                               |                            |
|                    |                    |                               |                            |
+--------------------+--------------------+-------------------------------+----------------------------+

# Budget

See [budget.ods](budget.ods).
