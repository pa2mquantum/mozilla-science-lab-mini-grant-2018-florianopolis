# Letter of Intent

Based on https://docs.google.com/document/d/1qPt0NWDIBmZ10F9bsWPGbDeZfy6pl2k177fV4jSoddo/edit#bookmark=kix.ubtjxm4cw744.

## General Information

### To which grant program are you responding?

Mozilla Science Mini Grants.

### If you were invited to apply by a Mozilla staff member, please provide their name.

**Skip this question**.

### Individual Applicant

**Yes**. Please check the box.

### Type of Support

Project Support.

### Fiscal Sponsor

No.

## In one sentence, summarize the purpose of the grant

We will create and pilot a 12-hours long open curriculum in Portuguese in Florianópolis, Brazil, covering Python and targeting Oceanography, Biology, Geosciences 
and Meteorology researchers who are looking to be more efficient and open by using Python to automate part of their research.

## Project Summary

### Project Title

PythonPolis: Python and Open Science Tools for Scientists

### What is the total project budget? (in USD)

6000

### How much money are you requesting (in USD)

5000

### Anticipated project start date

April 1, 2018. 

### Duration of the project in months

6

### Project Description

In 2016, Melissa Weber Mendonca co-organized and taught a "Python workshop for Oceanographers and Biologists" in Florianópilis, Brazil, which had a demand three 
times higher than the computing lab capacity. 

Given this unfulfilled demand, in this project we will:

- coordinate a lesson curriculum creating sprint, extending and specializing the previous material,
- release a lesson curriculum covering the basics of Python and related libraries, when relevant providing examples and applications for researchers in areas such as Oceanography, Biology, Geosciences and Meteorology, and
- run a 12-hour pilot workshop using the curriculum to acommodate the high demand for computational training in research. 

The new curriculum will be available in Portuguese tackling one of the barriers to enter the computer field: the lack of material in Portuguese, as reported 
by Andy Oram at "Open Source in Brazil". It will be accessible through a repository and under an appropriate open license.

We anticipate that the pilot workshop will have a dramatic impact on the type of work researchers are doing, their confidence and their continued learning 
of these skills, based on the feedback from similar workshops.

The lesson curriculum will cover the use of standard libraries for the development of scientific software (e.g. Numpy, SciPy, pandas, matplotlib) while also will encourage best practices when dealing with software and data in research, highlighting the challenges and benefits of appropriate data and software management to support reproducible research. We also explore the use of Jupyter Notebooks to this end. When relevant, we will highlight useful standards and tools for researchers in the different domains (specifically: Oceanography, Biology, Geosciences and Meteorology). For this, we will tap into Tania Allard's, Alejandra Gonzalez-Beltran's and Filipe Fernandes's expertise.

If funded, this project will provide the much-needed foundation for Melissa to pitch local funds to continue teaching the new curriculum, and promote a culture change 
around collaborative and open lesson development in the Brazilian research community, as well as promoting the role of software and research software engineers within the research environment.

### On which Internet health issue does your project focus. Chose one or more of the following:

- Open Innovation
- Digital Inclusion
- Web Literacy
